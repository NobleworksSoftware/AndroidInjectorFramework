package com.github.nobleworks.injection;

import android.content.Context;

public interface InjectorFactory
{
    String FACTORY = InjectorFactory.class.getCanonicalName();

    <T> T getInjector(Context context, Injectable<T> instance);
}
