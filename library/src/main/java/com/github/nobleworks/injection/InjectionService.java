package com.github.nobleworks.injection;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;

@SuppressWarnings("ResourceType")
public abstract class InjectionService
{
    public static <T extends InjectionService>
    Context initialize(Application application, Context baseContext, Class<T> clazz)
    {
        return initialize(application, baseContext, clazz.getCanonicalName());
    }

    public static Context initialize(Application application, Context baseContext, int classNameResource)
    {
        return initialize(application, baseContext, baseContext.getString(classNameResource));
    }

    public static Context initialize(Application application, Context baseContext)
    {
        ApplicationInfo applicationInfo = null;

        try
        {
            applicationInfo = baseContext.getPackageManager().getApplicationInfo(
                    baseContext.getPackageName(), PackageManager.GET_META_DATA);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            throw new Error("Could not instantiate InjectionFactory", e);
        }

        String injectorClass = applicationInfo.metaData.getString(InjectorFactory.FACTORY);

        return initialize(application, baseContext, injectorClass);
    }

    public static Context initialize(Application application, Context baseContext, String className)
    {
        if(getInjectorFactory(baseContext) == null)
        {
            try
            {
                return new InjectorFactoryContextWrapper(baseContext, createFactory(application, baseContext, className));
            }
            catch (Exception e)
            {
                throw new Error("Could not instantiate InjectionFactory", e);
            }
        }
        else
        {
            return baseContext;
        }
    }

    private static InjectorFactory createFactory(Application application, Context context, String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        InjectorFactory injectorFactory = create(context, className);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            application.registerActivityLifecycleCallbacks(
                    new InjectionServiceLifecycleCallbacks(injectorFactory));
        }

        inject(application, application, injectorFactory);

        return injectorFactory;
    }

    private static InjectorFactory create(Context context, String className)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        if(className == null || TextUtils.getTrimmedLength(className) == 0)
        {
            throw new IllegalStateException("No injector factory was specified");
        }
        else
        {
            return create(new ComponentName(context.getPackageName(), className));
        }
    }

    @SuppressWarnings("unchecked")
    private static InjectorFactory create(ComponentName name)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        String className = name.getShortClassName();

        if(className.startsWith("."))
        {
            className = name.getPackageName() + className;
        }

        return ((Class<? extends InjectorFactory>)Class.forName(className)).newInstance();
    }

    private static InjectorFactory getInjectorFactory(Context context)
    {
        return (InjectorFactory)context.getSystemService(InjectorFactory.FACTORY);
    }

    public static void inject(Activity activity)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            inject(activity, activity);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void inject(Fragment fragment)
    {
        inject(fragment.getActivity(), fragment);
    }

    public static void inject(android.support.v4.app.Fragment fragment)
    {
        inject(fragment.getActivity(), fragment);
    }

    public static void inject(View view)
    {
        inject(view.getContext(), view);
    }

    public static void inject(Context context, Object instance)
    {
        if(instance instanceof Injectable)
        {
            inject(context, (Injectable<?>)instance);
        }
    }

    public static <T> void inject(Context context, Injectable<T> instance)
    {
        inject(context, instance, getInjectorFactory(context.getApplicationContext()));
    }

    public static void inject(Context context, Object instance, InjectorFactory factory)
    {
        if(instance instanceof Injectable)
        {
            inject(context, (Injectable<?>)instance, factory);
        }
    }

    private static <T> void inject(Context context, Injectable<T> instance, InjectorFactory factory)
    {
        instance.acceptInjector(factory.getInjector(context, instance));
    }
}