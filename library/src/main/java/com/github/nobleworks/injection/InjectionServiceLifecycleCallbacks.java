package com.github.nobleworks.injection;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
class InjectionServiceLifecycleCallbacks implements Application.ActivityLifecycleCallbacks
{
    private final InjectorFactory injectorFactory;

    InjectionServiceLifecycleCallbacks(InjectorFactory injectorFactory)
    {
        this.injectorFactory = injectorFactory;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState)
    {
        InjectionService.inject(activity, activity, injectorFactory);
    }

    @Override
    public void onActivityStarted(Activity activity)
    {
    }

    @Override
    public void onActivityResumed(Activity activity)
    {
    }

    @Override
    public void onActivityPaused(Activity activity)
    {
    }

    @Override
    public void onActivityStopped(Activity activity)
    {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState)
    {
    }

    @Override
    public void onActivityDestroyed(Activity activity)
    {
    }
}
