package com.github.nobleworks.injection;

import android.content.Context;
import android.content.ContextWrapper;

class InjectorFactoryContextWrapper extends ContextWrapper
{
    private final InjectorFactory injectorFactory;

    public InjectorFactoryContextWrapper(Context base, InjectorFactory injectorFactory)
    {
        super(base);

        this.injectorFactory = injectorFactory;
    }

    @Override
    public Object getSystemService(String name)
    {
        return InjectorFactory.FACTORY.equals(name)? injectorFactory : super.getSystemService(name);
    }
}
