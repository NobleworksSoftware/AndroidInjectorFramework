package com.github.nobleworks.injection

import android.content.Context
import android.content.ContextWrapper

class InjectorFactoryContextWrapper(base: Context, private val injectorFactory: InjectorFactory)
    : ContextWrapper(base)
{
    override fun getSystemService(name: String): Any
            = if (InjectorFactory.FACTORY == name) injectorFactory else super.getSystemService(name)
}
