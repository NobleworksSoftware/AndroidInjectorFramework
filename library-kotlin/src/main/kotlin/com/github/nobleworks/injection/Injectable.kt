package com.github.nobleworks.injection

/**
 * Interface that must be applied to classes to allow them to be injected using the well-known
 * Visitor design pattern.
 * @param  The interface to the object that will be doing the injection.
 */
public interface Injectable<Injector>
{
    public fun acceptInjector(injector: Injector)
}
