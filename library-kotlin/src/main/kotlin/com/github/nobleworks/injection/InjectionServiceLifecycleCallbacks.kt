package com.github.nobleworks.injection

import android.annotation.TargetApi
import android.app.Activity
import android.app.Application
import android.os.Build
import android.os.Bundle

TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
class InjectionServiceLifecycleCallbacks(private val injectorFactory: InjectorFactory)
    : Application.ActivityLifecycleCallbacks
{
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?)
            = InjectionService.inject(activity, activity, injectorFactory)

    override fun onActivityStarted(activity: Activity) { }

    override fun onActivityResumed(activity: Activity) { }

    override fun onActivityPaused(activity: Activity) { }

    override fun onActivityStopped(activity: Activity) { }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) { }

    override fun onActivityDestroyed(activity: Activity) { }
}
