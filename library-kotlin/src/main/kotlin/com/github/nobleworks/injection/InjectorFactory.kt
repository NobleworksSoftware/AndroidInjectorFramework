package com.github.nobleworks.injection

import android.content.Context

public interface InjectorFactory
{
    fun <T> getInjector(context: Context, instance: Injectable<T>): T

    companion object
    {
        public val FACTORY: String = javaClass<InjectorFactory>().getCanonicalName()
    }
}
