package com.github.nobleworks.injection

import android.app.Activity
import android.app.Application
import android.app.Fragment
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.text.TextUtils
import android.view.View
import android.support.v4.app.Fragment as SupportFragment

SuppressWarnings("ResourceType")
public object InjectionService
{
    public fun initialize(application: Application, baseContext: Context,
            clazz: Class<out InjectionService>): Context
    {
        return initialize(application, baseContext, clazz.getCanonicalName())
    }

    public fun initialize(application: Application, baseContext: Context, classNameResource: Int): Context
    {
        return initialize(application, baseContext, baseContext.getString(classNameResource))
    }

    public fun initialize(application: Application, baseContext: Context): Context
    {
        val applicationInfo = baseContext.getPackageManager().getApplicationInfo(
                baseContext.getPackageName(), PackageManager.GET_META_DATA)
        val injectorClass = applicationInfo.metaData.getString(InjectorFactory.FACTORY)

        return initialize(application, baseContext, injectorClass)
    }

    public fun initialize(application: Application, baseContext: Context, className: String?): Context
        = if (getInjectorFactory(baseContext) == null)
        {
            InjectorFactoryContextWrapper(baseContext, createFactory(application, baseContext, className))
        }
        else
        {
            baseContext
        }

    private fun createFactory(application: Application, context: Context, className: String?): InjectorFactory
    {
        val injectorFactory = create(context, className)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            application.registerActivityLifecycleCallbacks(
                    InjectionServiceLifecycleCallbacks(injectorFactory))
        }

        inject(application, application, injectorFactory)

        return injectorFactory
    }

    private fun create(context: Context, className: String?): InjectorFactory
        = if (className == null || TextUtils.getTrimmedLength(className) == 0)
        {
            throw IllegalStateException("No injector factory was specified")
        }
        else
        {
            create(ComponentName(context.getPackageName(), className))
        }

    suppress("UNCHECKED_CAST")
    private fun create(name: ComponentName) : InjectorFactory
    {
        var className = name.getShortClassName()

        if (className.startsWith('.'))
        {
            className = name.getPackageName() + className
        }

        return (Class.forName(className) as Class<out InjectorFactory>).newInstance()
    }

    private fun getInjectorFactory(context: Context)
            = context.getSystemService(InjectorFactory.FACTORY) as? InjectorFactory

    public fun inject(activity: Activity)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            inject(activity, activity)
        }
    }

    public fun inject(fragment: Fragment)
    {
        inject(fragment.getActivity(), fragment)
    }

    public fun inject(fragment: SupportFragment)
    {
        inject(fragment.getActivity(), fragment)
    }

    public fun inject(view: View)
    {
        inject(view.getContext(), view)
    }

    public fun inject(context: Context, instance: Any)
    {
        if (instance is Injectable<*>)
        {
            inject(context, instance, getInjectorFactory(context)!!)
        }
    }

    public fun inject(context: Context, instance: Any, factory: InjectorFactory)
    {
        if (instance is Injectable<*>)
        {
            inject(context, instance, factory)
        }
    }

    private fun <T> inject(context: Context, instance: Injectable<T>, factory: InjectorFactory)
    {
        instance.acceptInjector(factory.getInjector(context, instance))
    }
}

private fun Activity.inject() { InjectionService.inject(this, this) }

private fun Fragment.inject() { InjectionService.inject(getActivity(), this) }

private fun SupportFragment.inject() { InjectionService.inject(this.getActivity(), this) }

private fun View.inject() { InjectionService.inject(getContext(), this) }
