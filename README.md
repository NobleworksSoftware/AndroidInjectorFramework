# Android Injection Framework

An abstraction for injection into Android classes like Activity so that the injected class contains
no knowledge of what is doing the injection. Was designed for use with dagger 2 but is entirely
independent of dagger and has no dependency on it.

## How to Use

There are 2 interfaces that your code must implement.

### Injectable
This interface must be implemented for any Application, Activity, Fragment, or View class
that needs injection.

    public interface Injectable<Injector>
    {
        void acceptInjector(Injector injector);
    }

Any class that does not implement this interface will not be injected. The <code>Injector</code>
type is an interface that you declare for doing the injection. This is an implementation of the
Visitor design pattern where <code>Injector</code> corresponds to the Visitor in the design pattern
and the method defined in this interface is the accept method from the pattern.

Here is how it would typically be used:

    public MyActivity extends AppCompatActivity implements Injectable<MyActivity.Injector>
    {
        public interface Injector { void inject(MyActivity activity); }

        public void acceptInjector(Injector injector) { injector.inject(this) }

This will just be boilerplate code that you put into each injectable class. For Dagger 2 then
these Injector interfaces are implemented by your components as in:

    @Singleton Component(modules = RootModule.class)
    public abstract class MyApplicationComponent
        :   MyActivity.Injector, MyOtherActivity.Injector, ...
    {

Alternatively you could combine those inject methods into a single interface used by multiple
classes but that eliminates isolation of one class from another as each class is tied to the
common interface which is tied to all other injectable classes.

### InjectorFactory

This is the interface your code must implement to get an implementation of the Injector interface
for a particular instance. For Dagger 2 this would be creating or returning a component instance
that implements the correct <code>Injector</code> interface for that class.

Here is an example of the simple case of a single component for the entire application.

    public interface MyInjectorFactory implements InjectorFactory
    {
        MyApplicationComponent component;

        <T> T getInjector(Context context, Injectable<T> instance)
        {
            if(component == null)
            {
                component = DaggerMyApplicationComponent.builder()
                    .build();
            }

            return component;
        }
    }

How about an application with different scopes corresponding to different Dagger components? There
must be a way to determine the scope from the instance. And easy way to do that would be to use an
interface. For example let's say some activities have a user scope which requires a userId value.
You might declare an interface like this that your activity implements:

    public interface HasUserScope
    {
        String getUserId();
    }

Your Injector factory can then check for this interface and choose a different component:

    if(instance instanceOf HasUserScope)
    {
        String userId = ((HasUserScope)instance).getUserId();
        return getUserComponent(userId);
    }
    else ...

## Setup
You need to be able to tell the InjectionService what class is implementing InjectorFactory when you
initialize it. The initialization is done by overriding the <code>attachBaseContext</code> method
in your <code>Application</code> class. There are 4 ways you can specify the class name of your
InjectorFactory. In all these case you can either specify the completely qualified class name or
by specifying it relative to the app package (e.g. ".core.MyInjectorFactory").

### Using metadata in the AndroidManifest

To do this you would add a meta-data element inside the application element of you Android Manifest:

    <meta-data
        android:name="com.github.nobleworks.injection.InjectorFactory"
        android:value=".core.MyInjectorFactory"/>

And then in your <code>Application</code> you would add this:

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(InjectionService.initialize(this, base));
    }

### Using a string resource

To do this you would define a string resource something like this:

    <string name="injectorFactory">.core.MyInjectorFactory</string>

And then in your <code>Application</code> you would add this:

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(InjectionService.initialize(this, base,
            R.string.injectorFactory));
    }

### Using a class instance

To do this you would add this in your <code>Application</code>:

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(InjectionService.initialize(this, base,
            MyInjectorFactory.class));
    }

### Using a string

To do this you would add this in your <code>Application</code>:

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(InjectionService.initialize(this, base,
            ".core.MyInjectorFactory"));
    }

## How to perform injection for each type

### For the Application

The initialize call will automatically perform injection on the <code>Application</code> so there
is nothing else to do.

### For Activities

When running on devices with API level of 14 (Ice Cream Sandwich) or above hooks
are installed to automatically do injection for all activities. If your app will
only run on devices with an API level of 14 or higher you do not need to do anything
to inject your activities other than have the Activity implement <code>Injectable</code>.

If your app supports the < 7% of users using older devices then you will have to
manually tell the InjectionService to inject the <code>Activity</code> using a call
like this, probably placed in your <code>onCreate</code> method (ideally in a base
class):

    InjectionService.inject(this);

If using Kotlin and the Kotlin version of this library, extension methods are provided
so that can be shortened to just:

    inject()

This call will do nothing on devices running Ice Cream Sandwich or newer so is safe to
call on all devices, but is only needed for supporting Honeycomb or earlier devices.

### For Fragments and Views

There is no way to automatically do the injection so you must manually ask for injection like this:

    InjectionService.inject(this);

If using Kotlin and the Kotlin version of this library, extension methods are provided
so that can be shortened to just:

    inject()
